<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect()->route('forum.index');
});

Route::auth();

Route::get('/forum', [
	'as' => 'forum.index',
	'uses' => 'HomeController@index'
]);

Route::get('/sample-templates', 'ProfileController@template');