# Prototype Forum

VALAR MORGHULIS :P

## Installation
- Install Composer https://getcomposer.org/
- run on your command prompt/terminal: 
	composer update (run this command within project folder)
- import database forum.sql file (located in database folder)
- create .env file in root folder(project folder)
-  just copy .env.example file's content to .env file

## Security Vulnerabilities and Bugs

If you discover a security vulnerability or bug within this project, please send an e-mail to Jopper Bardilas at 99blueskyflakes@gmail.com. All security vulnerabilities and bugs will be promptly addressed.

## For Suggestions or Questions?

email: 99blueskyflakes@gmail.com / jopper_jan@outlook.com


